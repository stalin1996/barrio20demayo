@extends('layouts.padre')
@section('titulo')
<h1><a class="navbar-brand" href="{{ url('/') }}"> Barrio 20 de Mayo</a></h1>
@stop
@section('contenido')
<!-- //header -->
<!-- icons -->
	<div class="w3_wthree_agileits_icons main-grid-border">
		<div class="container">
			 <div class="agileits_heading_section w3-heading-all">
				<h3>Servicios Básicos</h3>
			</div>
			<div class="grid_3 grid_4 w3_agileits_icons_page">
				<div class="icons">
					<section id="new">
						<a href="{{ url('luz') }}"><h3 class="page-header page-header icon-subheading">Luz </h3></a>
						<a href="{{ url('agua') }}"><h3 class="page-header page-header icon-subheading">Agua </h3></a>
						<a href="{{ url('telefono') }}"><h3 class="page-header page-header icon-subheading">Telefono </h3></a>
					</section>
				</div>
			</div>
		</div>	
	</div>
	<!-- //icons -->
@stop






