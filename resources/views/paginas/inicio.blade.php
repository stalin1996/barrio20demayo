@extends('layouts.padre')
@section('titulo')
<h1><a class="navbar-brand" href="{{ url('/') }}"> Barrio 20 de Mayo</a></h1>
@stop
@section('contenido')
   <div class="blog_sec">
        <div class="w3-heading-all">
                <h3>Galería</h3>
                </div>
        <div class="col-md-6 banner-btm-left">
            <div class="banner-btm-top">
                <div class="banner-btm-inner a1" id="a1">
                </div>
                <div class="banner-btm-inner a2" id="a2">

                </div>
            </div>
            <div class="banner-btm-bottom">
                <div class="banner-btm-inner a3" id="a3">

                </div>
                <div class="banner-btm-inner a4" id="a4">

                </div>
            </div>
        </div>
        <div class="col-md-6 banner-btm-left">
            <div class="banner-btm-top">
                <div class="banner-btm-inner a5" id="a5">
                </div>
                <div class="banner-btm-inner a6" id="a6">

                </div>
            </div>
            <div class="banner-btm-bottom">
                <div class="banner-btm-inner a7" id="a7">
                                
                </div>
                <div class="banner-btm-inner a8" id="a8">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <!--//blog-->

<!-- testimonials -->
    <div class="testimonials" id="testimonials">
        <div class="container">
            <div class="w3_agile_team_grid">
                
                <div class="w3-heading-all">
                    <head></head>
                    <h3>Directiva de <span>Barrio</span></h3>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="w3ls_testimonials_grids">
                 <section class="center slider">
                        <div class="agileits_testimonial_grid">
                            <div class="w3l_testimonial_grid">
                                <p>In eu auctor felis, id eleifend dolor. Integer bibendum dictum erat, 
                                    non laoreet dolor.</p>
                                <h4>Rosy Crisp</h4>
                                <h5>Student</h5>
                                <div class="w3l_testimonial_grid_pos">
                                    <img src="{{ asset('admin/plantilla/images/t1.jpg') }}" alt=" " class="img-responsive" />
                                </div>
                            </div>
                        </div>
                        <div class="agileits_testimonial_grid">
                            <div class="w3l_testimonial_grid">
                                <p>In eu auctor felis, id eleifend dolor. Integer bibendum dictum erat, 
                                    non laoreet dolor.</p>
                                <h4>Laura Paul</h4>
                                <h5>Student</h5>
                                <div class="w3l_testimonial_grid_pos">
                                    <img src="{{ asset('admin/plantilla/images/t2.jpg') }}" alt=" " class="img-responsive" />
                                </div>
                            </div>
                        </div>
                        <div class="agileits_testimonial_grid">
                            <div class="w3l_testimonial_grid">
                                <p>In eu auctor felis, id eleifend dolor. Integer bibendum dictum erat, 
                                    non laoreet dolor.</p>
                                <h4>Michael Doe</h4>
                                <h5>Student</h5>
                                <div class="w3l_testimonial_grid_pos">
                                    <img src="{{ asset('admin/plantilla/images/t3.jpg') }}" alt=" " class="img-responsive" />
                                </div>
                            </div>
                        </div>
                </section>
            </div>
        </div>
    </div>
@stop
