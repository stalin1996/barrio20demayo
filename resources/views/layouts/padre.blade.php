<!DOCTYPE html>
<html lang="zxx">
<head>
<title>Inicio</title>
    <!--/metadata -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">
<meta name="keywords" content="Best Look Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smart phone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //custom-theme -->


<link href="{{ asset('admin/plantilla/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />

<link href="{{ asset('admin/plantilla/css/JiSlider.css') }}" rel="stylesheet"> 
<link href="{{ asset('admin/plantilla/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />

<link href="{{ asset('admin/plantilla/css/sweetalert2.css') }}" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome-icons -->

<link href="{{ asset('admin/plantilla/css/font-awesome.css') }}" rel="stylesheet"> 
<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
<body>
<!-- header -->
<div class="banner-top" id="home">
    <div class="header">

    <div class="container">
            <div class="header-left">
                <div class="agileinfo-phone">
                    <!--<p><i class="fa fa-volume-control-phone" aria-hidden="true"></i> +1 234 567 8901</p>-->
                </div>
                <div class="agileinfo-phone agileinfo-map">
                    <!--<p><i class="fa fa-map-marker" aria-hidden="true"></i> 4th block,New York City.</p>-->
                </div>
                <div class="search-grid">
                    <form action="#" method="post">
                        <input type="text" name="subscribe" placeholder="Search" class="big-dog" name="Subscribe" required="">
                        <button class="btn1"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
                <div class="clearfix"> 
                      <a class="btn btn-primary" href="{{ url('logout') }}" style="color: white; font-size: 150%;">Cerrar sesion</a></div>


            </div>

         

        </div> 
           
        </div>

    <div class="w3_navigation">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header navbar-left">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    @yield('titulo')
                    
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <nav class="menu menu--iris">
                        <ul class="nav navbar-nav menu__list">
                            <li class="menu__itemt menu__item--current"><a href="{{ url('/') }}" class="menu__link">Inicio</a></li>
                            <li class="dropdown menu__item">
                                <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown">Servicio Comunitario<b class="caret"></b></a>
                                <ul class="dropdown-menu agile_short_dropdown">
                                    <li><a href="{{ url('sanitarios') }}">Servicio Sanitarios</a></li>
                                    <li><a href="{{ url('basicos') }}">Servicios Básicos</a></li>
                                </ul>
                            </li>
                            <!--
                            <li class="menu__item"><a href="about.html" class="menu__link">Noticias</a></li>
                            <li class="menu__item"><a href="contact.html" class="menu__link">Contacto</a></li>
                        -->
                        </ul>
                    </nav>
                </div>
            </nav>

        </div>
    </div>
<!-- //header -->
<!-- banner -->
 @yield('contenido')
<!-- //testimonials -->
<!-- footer -->

<div class="copy-right wow fadeInLeft animated" data-wow-delay=".5s">
    <div class="container">
            <p> &copy; 2018 Best Look . All Rights Reserved | Design by  <a href="http://w3layouts.com/"> W3layouts</a></p>
    </div>
</div>
<!-- //footer -->

<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

    <!-- js -->

<script src="{{ asset('admin/plantilla/js/jquery-2.1.4.min.js') }}"></script> 
<script src="{{ asset('admin/plantilla/js/JiSlider.js') }}"></script>
        <script>
            $(window).load(function () {
                $('#JiSlider').JiSlider({color: '#fff', start: 3, reverse: false}).addClass('ff')
            })
        </script>
<!-- carousal -->

    <script src="{{ asset('admin/plantilla/js/slick.js') }}" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $(document).on('ready', function() {
          $(".center").slick({
            dots: true,
            infinite: true,
            centerMode: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    arrows: true,
                    centerMode: false,
                    slidesToShow: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    arrows: true,
                    centerMode: false,
                    centerPadding: '40px',
                    slidesToShow: 1
                  }
                }
             ]
          });
        });
    </script>
<!-- //carousal -->

    <script src="{{ asset('admin/plantilla/js/SmoothScroll.min.js') }}"></script>
<!-- start-smoth-scrolling -->
<!-- //for bootstrap working -->

<!-- start-smooth-scrolling -->

<script type="text/javascript" src="{{ asset('admin/plantilla/js/move-top.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/plantilla/js/easing.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){     
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smooth-scrolling -->
<!-- here stars scrolling icon -->
    <script type="text/javascript">
        $(document).ready(function() {
            /*
                var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
                };
            */
                                
            $().UItoTop({ easingType: 'easeOutQuart' });
                                
            });
    </script>
<!-- //here ends scrolling icon -->

<script type="text/javascript" src="{{ asset('admin/plantilla/js/bootstrap.js') }}"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 

<script type="text/javascript" src="{{ asset('admin/plantilla/js/sweetalert2.min.js') }}"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
</body>
</html>
<script type="text/javascript">
    $('.banner-btm-inner').click(
        function(){
            var id = this.id;
            var Html = `<div class="${id}"></div>`
            swal({
                title: 'Imagen',
                html: Html,
                showCloseButton: true,
            })
        })
</script>