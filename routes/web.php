<?php

/*
Route::get('/', function () {
    return view('paginas.login');
});
*/
Route::get('/','paginasController@inicio');
Route::get('sanitarios','paginasController@sanitarios');
Route::get('basicos','paginasController@basicos');
Route::get('luz','paginasController@luz');
Route::get('agua','paginasController@agua');
Route::get('telefono','paginasController@telefono');
Route::resource('login','loginController');
Route::post('login',['as'=>'login','uses'=>'loginController@store']);
Route::resource('registro','registroController');
Route::get('logout', 'loginController@logout');