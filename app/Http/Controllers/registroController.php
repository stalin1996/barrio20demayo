<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\usuarioModel;
use Session;
use Redirect;
class registroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('paginas.registro');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
             $this->validate($request, [
            'email' => 'required|email|unique:usuario',   
            'username' => 'required|unique:usuario',     
            'pass' => 'required',                
            ],            
            [
              'email.required'=>'El correo electronico es requerido',
              'username.required'=>'El nombre de usuario es requerido',
              'email.password'=>'La contraseña es requerida',
            ]
            
          );
        $usuario=new usuarioModel();
        $usuario->email=$request->email;
        $usuario->username=$request->username;
        $usuario->password=bcrypt($request->pass);
        $usuario->save();
        Session::flash('message','Usuario registrado correctamente, puede iniciar sesion');            
        return Redirect('login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
