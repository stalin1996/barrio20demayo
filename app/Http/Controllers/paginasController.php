<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;

class paginasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    function inicio()
    {
    	return View('paginas.inicio');
    }

     function sanitarios()
    {
    	return View('paginas.sanitarios');
    }

     function basicos()
    {
    	return View('paginas.basicos');
    }
     function luz()
    {
    	return View('paginas.luz');
    }
    function agua()
    {
    	return View('paginas.agua');
    }
    function telefono()
    {
    	return View('paginas.telefono');
    }
}
